package ru.mai.dep810.sample;

public class JavaSample {

    // Статическая переменная - строка.
    static String wordString = "Java is a computer programming language " +
            "that allows programs to run on a number of different types of " +
            "computers and/or operating systems";

    /**
     * Функция main - точка входа в java - программу.
     *
     * @param args - аргументы командной строки.
     */
    public static void main(String[] args) {

        Object[] arr = new Object[100000];
        for (int i=0; i<arr.length; i++) {
            arr[i] = new Object[100000];
        }

        // Объявление массива строк.
        // Получим массив строк из предложения, разделив его на слова.
        String[] wordArray = wordString.split(" ");

        // Цикл - перебор массива. Массив тоже является объектом.
        for (int i = 0; i < wordArray.length; i++) {
            // Вывод на консоль текущего элемента массива
            System.out.println("wordArray[" + i + "] = " + wordArray[i]);
        }

        // Сортировка массива. Статический метод sort класса Arrays.
        java.util.Arrays.sort(wordArray);

        // Краткий вариант цикла по массиву:
        // "для каждого word из wordArray"
        for (String word : wordArray) {
            System.out.print(" [" + word + "] ");
        }

        // Вывод на консоль окончания строки.
        System.out.println();
    }

}
