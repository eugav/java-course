package ru.mai.dep810.sample;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Quantity {
    private final BigDecimal amount;
    private final UnitOfMeasure unit;

    public Quantity(String amount, UnitOfMeasure unit) {
        this.amount = new BigDecimal(amount).setScale(3, RoundingMode.HALF_EVEN);
        this.unit = unit;
    }

    private Quantity(BigDecimal amount, UnitOfMeasure unit) {
        this.amount = amount;
        this.unit = unit;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public UnitOfMeasure getUnit() {
        return unit;
    }

    /**
     * Складывает два объекта Quantity.
     * @param quantity слагаемое.
     * @return результат сложения.
     */
    public Quantity add(Quantity quantity) {
        if (this.unit != quantity.unit) {
            throw new CannotConvertUnitOfMeasureException(
                    "Единицы измерения не совпадают " + this.unit +
                    " " + quantity.unit);
        }
        return new Quantity(this.amount.add(quantity.amount), this.unit);
    }

    @Override
    public String toString() {
        return amount + " " + UnitOfMeasure.KG;
    }
}
