package ru.mai.dep810.sample;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Objects;

public class StringSample {

    public static final int PRODUCT_QUANTITY_SCALE = 3;

    public static void main(String[] args) {

        String a = "bcd";
        String b = new String("abc");

        System.out.println(Objects.equals(a, b));

        System.out.println(a + " / " + b);

        StringBuilder s = new StringBuilder("");
        for (int i = 0; i < 1000000; i++) {
            s.append(" ").append(i);
        }
        System.out.println(s);

        BigInteger bi = BigInteger.ZERO;
        new BigInteger("7");

        double d = 0.0;
        for (int i=0; i<10; i++) {
            d+=0.1;
        }
        System.out.println(d);

        BigDecimal bd = new BigDecimal("0.100")
                .setScale(PRODUCT_QUANTITY_SCALE, RoundingMode.HALF_EVEN);

//        bd.divideAndRemainder()
    }
}
