package ru.mai.dep810.sample;

public class QuantitySample {


    public static void main(String[] args) {

        Quantity q1 = new Quantity("5", UnitOfMeasure.KG);

        try {
            System.out.println(q1.add(new Quantity("7", UnitOfMeasure.METER)));
        } catch (CannotConvertUnitOfMeasureException e) {
            e.printStackTrace();
        }

        UnitOfMeasure.valueOf("KG");

    }
}
