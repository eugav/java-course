package ru.mai.dep810.sample;

import java.math.BigDecimal;

public enum UnitOfMeasure {
    KG(null, BigDecimal.ONE),
    TON(KG, new BigDecimal("1000")),
    METER(null, BigDecimal.ONE),
    SQ_METER(null, BigDecimal.ONE);

    private UnitOfMeasure base;
    private BigDecimal coeff;

    UnitOfMeasure(UnitOfMeasure base, BigDecimal coeff) {
        this.base = base;
        this.coeff = coeff;
    }

    public UnitOfMeasure getBase() {
        return base;
    }

    public BigDecimal getCoeff() {
        return coeff;
    }
}
