package ru.mai.dep810.sample;

public class CannotConvertUnitOfMeasureException extends RuntimeException {
    
    public CannotConvertUnitOfMeasureException(String message) {
        super(message);
    }

    public CannotConvertUnitOfMeasureException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotConvertUnitOfMeasureException(Throwable cause) {
        super(cause);
    }

    public CannotConvertUnitOfMeasureException() {
    }
}
